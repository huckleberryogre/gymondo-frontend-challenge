# gymondo-frontend-challenge

Frontend challenge for the Gymondo company

## Description

Server:
* in the root directory.
* NestJS + TypeORM.
* on the first run it will run migration to populate database with the mock data.
* dev server run on http://localhost:4000

Client:
* in the `client` directory.
* React + Redux Toolkit + Typescript + Semantic UI.
* dev server run on http://localhost:3000 and set up to proxy api requests to the dev server

## Installation

For both client and server in their respective folders:

```
yarn install
```

To run e2e you may need to download browsers drivers:

```
npx playwright install
```

In order to run TypeORM CLI (for migrations) you may need ts-node to be installed globally:

```
yarn add global ts-node
```

## Running the app [development]

Both client and server from their folders:

```
yarn run start
```

## Running client unit tests

```
yarn run test
```

## Running client e2e tests

```
yarn run test:e2e
```
