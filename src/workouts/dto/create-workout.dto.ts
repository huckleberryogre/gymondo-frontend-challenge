import {IsIn} from "class-validator";

/**
 * Create workout DTO.
 */
export class CreateWorkoutDto {
    name: string;
    description: string;
    startDate: number;
    @IsIn(['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7'])
    category: string;
}
