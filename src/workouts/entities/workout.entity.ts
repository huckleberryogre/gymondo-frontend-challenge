import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Workout {
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * Workout name
     * @example Running
     */
    @Column()
    name: string;

    /**
     * Workout description
     * @example Regular running 15 minutes
     */
    @Column()
    description: string;

    /**
     * Workout starting date timestamp with the local timezone.
     * @example 1629409699752
     */
    @Column()
    startDate: number;

    /**
     * Workout category
     * @example c3
     */
    @Column()
    category: string;
}
