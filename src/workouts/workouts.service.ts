import { Injectable } from '@nestjs/common';
import { CreateWorkoutDto } from './dto/create-workout.dto';
import { UpdateWorkoutDto } from './dto/update-workout.dto';
import {InjectRepository} from "@nestjs/typeorm";
import {Workout} from "./entities/workout.entity";
import {Repository} from "typeorm";
import {validate} from "class-validator";

@Injectable()
export class WorkoutsService {
  constructor(
      @InjectRepository(Workout)
      private readonly workoutsRepository: Repository<Workout>
  ) {}

  async create(createWorkoutDto: CreateWorkoutDto) {
    const workout = this.workoutsRepository.create(createWorkoutDto);
    const errors = await validate(workout);

    if (errors.length) {
      return errors;
    } else {
      return this.workoutsRepository.save(workout);
    }
  }

  findAll(): Promise<Workout[]> {
    return this.workoutsRepository.find();
  }

  findOne(id: number): Promise<Workout> {
    return this.workoutsRepository.findOne(id);
  }

  update(id: number, updateWorkoutDto: UpdateWorkoutDto) {
    return this.workoutsRepository.update(id, updateWorkoutDto);
  }

  async remove(id: number): Promise<void> {
     await this.workoutsRepository.delete(id);
  }
}
