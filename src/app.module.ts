import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {WorkoutsModule} from './workouts/workouts.module';
import {TypeOrmModule} from '@nestjs/typeorm';

const typeOrmModule = TypeOrmModule.forRoot({
    type: 'better-sqlite3',
    database: 'database/sqlite3.db',
    synchronize: true,
    migrationsRun: true,
    logging: ['info', 'migration', 'error', 'log', 'schema'],
    entities: ["dist/workouts/entities/*.entity.js"],
    autoLoadEntities: true,
    migrations: ['dist/database/migrations/*.js'],
    cli: {
        migrationsDir: "database/migrations/*.ts"
    }
});

@Module({
    imports: [typeOrmModule, WorkoutsModule],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
