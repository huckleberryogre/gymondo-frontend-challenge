import {createAsyncThunk, createSelector, createSlice} from '@reduxjs/toolkit';
import {RootState} from '../store';
import {WorkoutsAPI} from "../../api/workoutsAPI";
import {ERequestStatus} from "./enums";
import {IWorkoutsState} from "./models";

const initialState: IWorkoutsState = {
    allWorkouts: [],
    status: ERequestStatus.IDLE,
    page: 1,
    filter: {
        categories: [],
        date: [],
    }
};

export const fetchAllWorkouts = createAsyncThunk(
    'workouts/fetchAll',
    async () => {
        return await WorkoutsAPI.fetchAll();
    }
);

export const workoutsSlice = createSlice({
    name: 'workouts',
    initialState,
    reducers: {
        setPage: (state, action) => {
            state.page = action.payload;
        },
        setFilterCategories: (state, action) => {
            state.filter.categories = action.payload;
        },
        setFilterDate: (state, action) => {
            state.filter.date = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllWorkouts.rejected, (state) => {
                state.status = ERequestStatus.FAILED;
            })
            .addCase(fetchAllWorkouts.pending, (state) => {
                state.status = ERequestStatus.LOADING;
            })
            .addCase(fetchAllWorkouts.fulfilled, (state, action) => {
                state.status = ERequestStatus.SUCCESS;
                state.allWorkouts = action.payload;
            });
    },
});

export const selectWorkouts = (state: RootState) => state.workouts.allWorkouts;
export const selectWorkoutsFilter = (state: RootState) => state.workouts.filter;
export const selectWorkout = (id: number) => (state: RootState) => state.workouts.allWorkouts.find(workout => workout.id === id);
export const selectFilteredWorkouts = createSelector(
    [selectWorkouts, selectWorkoutsFilter],
    (workouts, filter) => {
        const {categories, date} = filter;
        let filteredWorkouts = [...workouts];
        let dates = date.map(d => new Date(d));

        if (categories.length) {
            filteredWorkouts = filteredWorkouts.filter(workout => categories.includes(workout.category));
        }

        if (dates.length === 1) {
            filteredWorkouts = filteredWorkouts.filter(workout => dates[0].toLocaleDateString('en-GB') === (new Date(
                workout.startDate)).toLocaleDateString('en-GB'));
        }
        if (dates.length === 2) {
            filteredWorkouts = filteredWorkouts.filter(workout => {
                const isWithinRange = workout.startDate >= date[0] && workout.startDate <= date[1];
                //edge case:
                const isLastDay = (new Date(workout.startDate)).toLocaleDateString('en-GB') === dates[1].toLocaleDateString(
                    'en-GB');

                return isWithinRange || isLastDay;
            });
        }

        return filteredWorkouts;
    }
);
export const selectWorkoutsTablePage = (state: RootState) => state.workouts.page;

export const workoutsReducer = workoutsSlice.reducer;
export const workoutActions = workoutsSlice.actions;
export * from "./models";
export * from "./enums";