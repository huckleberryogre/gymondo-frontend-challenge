import {ERequestStatus, EWorkoutCategory} from "./enums";

export interface IWorkout {
    id: number;
    name: string;
    description: string;
    startDate: number;
    category: EWorkoutCategory;
}

export interface IWorkoutFilter {
    categories: EWorkoutCategory[];
    // timestamps
    date: number[];
}

export interface IWorkoutsState {
    allWorkouts: IWorkout[];
    status: ERequestStatus;
    page: number;
    filter: IWorkoutFilter;
}
