/**
 * Workout category.
 */
export enum EWorkoutCategory {
    c1 = 'c1',
    c2 = 'c2',
    c3 = 'c3',
    c4 = 'c4',
    c5 = 'c5',
    c6 = 'c6',
    c7 = 'c7',
}

/**
 * Request status.
 */
export enum ERequestStatus {
    IDLE = 'IDLE',
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    FAILED = 'FAILED',
}
