import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import {workoutsReducer} from './workoutSlice';

export const store = configureStore({
  reducer: {
    workouts: workoutsReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
