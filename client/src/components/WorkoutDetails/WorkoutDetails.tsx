import {useSelector} from "react-redux";
import {selectWorkout} from "../../store";
import {useHistory, useParams} from "react-router-dom";
import {Button, Header, Icon, Item, Segment} from "semantic-ui-react";
import {getHumanReadableDateFromTimestamp} from "../../utilities/getHumanReadableDateFromTimestamp";

export const WorkoutDetails = () => {
    const history = useHistory();
    const {id} = useParams<{ id: string }>();
    const workout = useSelector(selectWorkout(+id));

    return workout ? (
        <Segment placeholder textAlign="center" data-testid="workout-details">
            <Header icon>
                <Icon name='heartbeat' />
            </Header>
            <Item.Group>
                <Item>
                    <Item.Content>
                        <Item.Header data-testid="workout-header">Workout #{workout.id} details</Item.Header>
                        <Item.Meta data-testid="workout-meta">{workout.name} | {workout.category}</Item.Meta>
                        <Item.Description data-testid="workout-description">
                            {workout.description}
                        </Item.Description>
                        <Item.Extra data-testid="workout-extra">Start date: {getHumanReadableDateFromTimestamp(workout.startDate)}</Item.Extra>
                    </Item.Content>
                </Item>
            </Item.Group>
            <Button basic icon labelPosition='left' onClick={history.goBack}>
                <Icon name='arrow left' />
                Back
            </Button>
        </Segment>
    ) : <span>workout not found</span>;
};
