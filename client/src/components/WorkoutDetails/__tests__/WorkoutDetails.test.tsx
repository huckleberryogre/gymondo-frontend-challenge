import React from 'react';
import {render} from '@testing-library/react';
import {Provider} from 'react-redux';
import {store} from "../../../store";
import {WorkoutDetails} from "../WorkoutDetails";
import {createMemoryHistory} from 'history';
import {Router, Switch, Route} from 'react-router-dom';
import {allWorkoutsMock} from "./__fixtures__";

test('should render workout not found state', () => {
    const history = createMemoryHistory();
    history.push('/workouts/7');

    const {getByText} = render(
        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route path="/workouts/:id" component={WorkoutDetails}/>
                </Switch>
            </Router>
        </Provider>
    );

    expect(getByText(/workout not found/i)).toBeInTheDocument();
});

test('should render normal workout page', () => {
    const history = createMemoryHistory();

    store.dispatch({
        type: 'workouts/fetchAll/fulfilled',
        payload: allWorkoutsMock,
    });

    history.push('/workouts/7');

    const {getByText} = render(
        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route path="/workouts/:id" component={WorkoutDetails}/>
                </Switch>
            </Router>
        </Provider>
    );

    expect(getByText(/Workout #7 details/i)).toBeInTheDocument();
    expect(getByText(/running | c1/i)).toBeInTheDocument();
    expect(getByText(/regular running/i)).toBeInTheDocument();
    expect(getByText(new RegExp('Start date: 20/08/2021', 'i'))).toBeInTheDocument();
});
