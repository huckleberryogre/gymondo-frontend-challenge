import React from "react";
import {Dropdown, Menu} from "semantic-ui-react";
import Datepicker from "react-semantic-ui-datepickers";
import {workoutCategoryOptions} from "../../App";
import {SemanticDatepickerProps} from "react-semantic-ui-datepickers/dist/types";
import {selectWorkoutsFilter, useAppDispatch, workoutActions} from "../../store";
import {DropdownProps} from "semantic-ui-react/dist/commonjs/modules/Dropdown/Dropdown";
import {useSelector} from "react-redux";

export const WorkoutsTableMenu = () => {
    const dispatch = useAppDispatch();
    const {date, categories} = useSelector(selectWorkoutsFilter);
    const {setFilterDate, setFilterCategories, setPage} = workoutActions;

    const handleChangeDate = (data: SemanticDatepickerProps) => {
        if (Array.isArray(data.value)) {
            dispatch(setFilterDate(data.value.map(date => +date)));
        } else if (data.value) {
            dispatch(setFilterDate([+data.value]));
        } else {
            dispatch(setFilterDate([]));
        }
        dispatch(setPage(1));
    };

    const handleChangeCategory = (_: React.SyntheticEvent<HTMLElement>, data: DropdownProps) => {
        dispatch(setFilterCategories(data.value));
        dispatch(setPage(1));
    }

    return (
        <Menu size="tiny" data-testid="workouts-menu">
            <Menu.Item>
                <Datepicker
                    type="range"
                    onChange={(_, data) => handleChangeDate(data)}
                    placeholder="start date"
                    format="DD/MM/YYYY"
                    value={date.map(d => new Date(d))}
                />
            </Menu.Item>
            <Menu.Item>
                <Dropdown
                    data-testid="category-select"
                    onChange={handleChangeCategory}
                    placeholder="category"
                    fluid
                    multiple
                    selection
                    options={workoutCategoryOptions}
                    value={categories}
                />
            </Menu.Item>
        </Menu>
    );
};