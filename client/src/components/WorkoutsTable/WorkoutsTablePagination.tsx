import React, {useMemo} from "react";
import {Icon, Pagination, PaginationProps} from "semantic-ui-react";
import {useSelector} from "react-redux";
import {
    selectFilteredWorkouts,
    selectWorkoutsTablePage,
    useAppDispatch,
    workoutActions
} from "../../store";
import {workoutItemsPerPage} from "../../constants";

export const WorkoutsTablePagination = () => {
    const dispatch = useAppDispatch();
    const workouts = useSelector(selectFilteredWorkouts);
    const page = useSelector(selectWorkoutsTablePage);
    const totalPages = Math.ceil(workouts.length / workoutItemsPerPage);
    const firstPageColor = page === 1 ? "grey" : undefined;
    const lastPageColor = page === totalPages ? "grey" : undefined;
    const ellipsisItem = useMemo(() => ({content: <Icon name="ellipsis horizontal"/>, icon: true}), []);
    const firstItem = useMemo(
        () => ({content: <Icon color={firstPageColor} name="angle double left"/>, icon: true}),
        [firstPageColor]
    );
    const prevItem = useMemo(
        () => ({content: <Icon color={firstPageColor} name="angle left"/>, icon: true}),
        [firstPageColor]
    );
    const nextItem = useMemo(
        () => ({content: <Icon color={lastPageColor} name="angle right"/>, icon: true}),
        [lastPageColor]
    );
    const lastItem = useMemo(
        () => ({content: <Icon color={lastPageColor} name="angle double right"/>, icon: true}),
        [lastPageColor]
    );

    const handlePageChange = (_: React.MouseEvent<HTMLAnchorElement>, data: PaginationProps) => {
        const {setPage} = workoutActions;
        dispatch(setPage(data.activePage));
    };

    return totalPages > 1 ? (
        <div className="pagination-container">
            <Pagination
                data-testid="workouts-pagination"
                activePage={page}
                ellipsisItem={ellipsisItem}
                firstItem={firstItem}
                prevItem={prevItem}
                nextItem={nextItem}
                lastItem={lastItem}
                totalPages={totalPages}
                onPageChange={handlePageChange}
            />
        </div>
    ) : null;
};