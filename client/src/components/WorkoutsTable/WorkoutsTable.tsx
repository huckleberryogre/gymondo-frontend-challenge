import React from "react";
import {Table} from "semantic-ui-react";
import {useSelector} from "react-redux";
import {selectFilteredWorkouts, selectWorkoutsTablePage} from "../../store";
import {workoutItemsPerPage} from "../../constants";
import {getHumanReadableDateFromTimestamp} from "../../utilities/getHumanReadableDateFromTimestamp";
import {useHistory} from "react-router-dom";

export const WorkoutsTable = () => {
    const history = useHistory();
    const workouts = useSelector(selectFilteredWorkouts);
    const page = useSelector(selectWorkoutsTablePage);
    const workoutsSlice = workouts.slice((page - 1) * workoutItemsPerPage, page * workoutItemsPerPage);

    const handleClickSelectWorkout = (workoutId: number) => () => {
        history.push(`/workouts/${workoutId}`);
    };

    return (
        <Table striped selectable data-testid="workouts-table">
            <Table.Header>
                <Table.Row textAlign="center">
                    <Table.HeaderCell>id</Table.HeaderCell>
                    <Table.HeaderCell>name</Table.HeaderCell>
                    <Table.HeaderCell>category</Table.HeaderCell>
                    <Table.HeaderCell>description</Table.HeaderCell>
                    <Table.HeaderCell>start date</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body data-testid="workouts-table-body">
                {workoutsSlice.map(workout => (
                    <Table.Row key={workout.id} onClick={handleClickSelectWorkout(workout.id)}>
                        <Table.Cell data-testid="workout-id" textAlign="center">{workout.id}</Table.Cell>
                        <Table.Cell data-testid="workout-name" textAlign="left">{workout.name}</Table.Cell>
                        <Table.Cell data-testid="workout-category" textAlign="center">{workout.category}</Table.Cell>
                        <Table.Cell data-testid="workout-description" textAlign="left">{workout.description}</Table.Cell>
                        <Table.Cell data-testid="workout-startDate" textAlign="center">
                            {getHumanReadableDateFromTimestamp(workout.startDate)}
                        </Table.Cell>
                    </Table.Row>
                ))}
            </Table.Body>
        </Table>
    );
};
