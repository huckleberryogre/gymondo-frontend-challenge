export const allWorkoutsMock21 = [
    {
        "id": 1,
        "name": "Circuit",
        "description": "Aperiam doloribus necessitatibus aut repudiandae quidem dolores dolorum aut corrupti.",
        "startDate": 1635931013260,
        "category": "c2"
    },
    {
        "id": 2,
        "name": "Dynamic Warm-Up",
        "description": "Quam ipsa commodi in fugiat tempora qui iusto ut.",
        "startDate": 1635516166767,
        "category": "c5"
    },
    {
        "id": 3,
        "name": "Steady-State Cardio",
        "description": "Consequuntur eius eum.",
        "startDate": 1633916580333,
        "category": "c6"
    },
    {
        "id": 4,
        "name": "HIIT",
        "description": "Non possimus quod sit.",
        "startDate": 1637180486415,
        "category": "c1"
    },
    {
        "id": 5,
        "name": "Cross-Training",
        "description": "Sint quas reprehenderit qui debitis velit qui.",
        "startDate": 1639941124999,
        "category": "c2"
    },
    {
        "id": 6,
        "name": "Cool-Down",
        "description": "Est a cum molestiae earum consequatur eos vero quo aliquid.",
        "startDate": 1631818193924,
        "category": "c3"
    },
    {
        "id": 7,
        "name": "Sets",
        "description": "Ut aut est iste ut est harum dolor consequatur.",
        "startDate": 1635998234591,
        "category": "c2"
    },
    {
        "id": 8,
        "name": "Aerobic Exercise",
        "description": "Itaque ex dicta eos vitae cum minus perferendis.",
        "startDate": 1639254519101,
        "category": "c4"
    },
    {
        "id": 9,
        "name": "Boot Camp",
        "description": "Libero nulla voluptatem omnis.",
        "startDate": 1637982464119,
        "category": "c7"
    },
    {
        "id": 10,
        "name": "Reps",
        "description": "Ducimus autem maxime cupiditate nemo cumque est id aut.",
        "startDate": 1640116955928,
        "category": "c4"
    },
    {
        "id": 11,
        "name": "Resistance",
        "description": "Minima sint voluptate dolor id sed.",
        "startDate": 1638898581150,
        "category": "c3"
    },
    {
        "id": 12,
        "name": "HIIT",
        "description": "Repudiandae consequatur nihil ratione aspernatur sint voluptate assumenda est qui.",
        "startDate": 1640309704773,
        "category": "c3"
    },
    {
        "id": 13,
        "name": "Interval Training",
        "description": "Rem placeat soluta ab non omnis qui velit.",
        "startDate": 1635731537257,
        "category": "c1"
    },
    {
        "id": 14,
        "name": "Isometrics",
        "description": "Dolor in voluptatem vel est autem debitis.",
        "startDate": 1636818218007,
        "category": "c3"
    },
    {
        "id": 15,
        "name": "Isometrics",
        "description": "Facere vero repellendus placeat officia quis.",
        "startDate": 1633206027905,
        "category": "c6"
    },
    {
        "id": 16,
        "name": "Reps",
        "description": "Eos dignissimos est minus quia ipsa.",
        "startDate": 1635371329342,
        "category": "c7"
    },
    {
        "id": 17,
        "name": "Sets",
        "description": "Minus velit et rerum similique soluta.",
        "startDate": 1638174793902,
        "category": "c1"
    },
    {
        "id": 18,
        "name": "Anaerobic Exercise",
        "description": "Sequi eos quam et praesentium ipsam dolorem facere ducimus ea.",
        "startDate": 1635545029653,
        "category": "c1"
    },
    {
        "id": 19,
        "name": "Sets",
        "description": "Modi ratione deleniti ea suscipit laboriosam ut alias officia.",
        "startDate": 1639336147330,
        "category": "c6"
    },
    {
        "id": 20,
        "name": "HIIT",
        "description": "Eos porro molestias error error.",
        "startDate": 1638727587909,
        "category": "c3"
    },
    {
        "id": 21,
        "name": "Boot Camp",
        "description": "Et quia at corporis.",
        "startDate": 1637452943149,
        "category": "c7"
    }
]