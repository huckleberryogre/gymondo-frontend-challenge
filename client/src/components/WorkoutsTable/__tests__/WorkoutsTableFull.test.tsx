import React from "react";
import {createMemoryHistory} from "history";
import {store} from "../../../store";
import {render} from "@testing-library/react";
import {Provider} from "react-redux";
import {Route, Router, Switch} from "react-router-dom";
import {allWorkoutsMock21} from "./__fixtures__";
import {WorkoutsTableFull} from "../WorkoutsTableFull";

test('should render proper full table', () => {
    const history = createMemoryHistory();

    store.dispatch({
        type: 'workouts/fetchAll/fulfilled',
        payload: allWorkoutsMock21,
    });

    history.push('/workouts');

    const {getByTestId} = render(
        <Provider store={store}>
            <Router history={history}>
                <Switch>
                    <Route path="/workouts" component={WorkoutsTableFull}/>
                </Switch>
            </Router>
        </Provider>
    );

    expect(getByTestId("workouts-menu")).toBeInTheDocument();
    expect(getByTestId("workouts-table")).toBeInTheDocument();
    expect(getByTestId("workouts-pagination")).toBeInTheDocument();

    expect(getByTestId("workouts-table-body").childElementCount).toEqual(20);
    expect(document.querySelector('.pagination .active.item')?.textContent).toEqual('1');
    expect(getByTestId("workouts-pagination").childElementCount).toEqual(6);
});