import React from "react";
import {WorkoutsTableMenu} from "./WorkoutsTableMenu";
import {WorkoutsTable} from "./WorkoutsTable";
import {WorkoutsTablePagination} from "./WorkoutsTablePagination";

export const WorkoutsTableFull = () => {
    return (
        <>
            <WorkoutsTableMenu />
            <WorkoutsTable />
            <WorkoutsTablePagination />
        </>
    )
}
