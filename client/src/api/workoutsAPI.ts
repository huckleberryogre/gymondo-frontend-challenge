/**
 * Workouts API.
 */
export const WorkoutsAPI = {
    fetchAll: async () => {
        return await fetch('/api/workouts').then(result => result.json());
    }
}