import React, {useEffect} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import logo from './assets/gymondo-logo.png';
import {Container} from "semantic-ui-react";
import {workoutCategories} from "./constants";
import {fetchAllWorkouts, useAppDispatch} from "./store";
import {WorkoutsTableFull, WorkoutDetails} from "./components";

export const workoutCategoryOptions = workoutCategories.map(category => ({
    key: category,
    text: category,
    value: category
}));

export const App = () => {
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(fetchAllWorkouts());
    }, [dispatch]);


    return (
        <div className="app-container">
            <Router>
                <header className="display-flex">
                    <span className="margin-auto">
                        <Link to="/">
                            <img src={logo} alt="logo"/>
                        </Link>
                    </span>
                </header>
                <Container>
                    <Switch>
                        <Route path="/workouts/:id" component={WorkoutDetails}/>
                        <Route path="/workouts" component={WorkoutsTableFull}/>
                        <Route path="/" component={WorkoutsTableFull}/>
                    </Switch>
                </Container>
            </Router>
        </div>
    );
};
