import {test, expect} from '@playwright/test';

/**
 * Happy path test suit.
 * Timeouts are intended just for the show.
 */
test('happy path test', async ({page}) => {
    await page.goto('http://localhost:3000');
    const workoutRowLocator = await page.locator('[data-testid="workouts-table-body"] tr:first-of-type');

    const workoutRow = await workoutRowLocator.elementHandle({timeout: 5000});

    const id = await (await workoutRow.$('[data-testid="workout-id"]')).innerText();
    const name = await (await workoutRow.$('[data-testid="workout-name"]')).innerText();
    const category = await (await workoutRow.$('[data-testid="workout-category"]')).innerText();
    const description = await (await workoutRow.$('[data-testid="workout-description"]')).innerText();
    const startDate = await (await workoutRow.$('[data-testid="workout-startDate"]')).innerText();

    await page.waitForTimeout(1000);

    await workoutRow.click();

    await page.waitForSelector('[data-testid="workout-details"]', {timeout: 5000});

    await expect(page.locator('[data-testid="workout-header"]')).toContainText(`Workout #${id} details`);
    await expect(page.locator('[data-testid="workout-meta"]')).toContainText(`${name} | ${category}`);
    await expect(page.locator('[data-testid="workout-description"]')).toContainText(description);
    await expect(page.locator('[data-testid="workout-extra"]')).toContainText(`Start date: ${startDate}`);

    await page.waitForTimeout(1000);

    await page.goBack();

    await page.waitForTimeout(1000);

    await (await page.waitForSelector('[data-testid="workouts-pagination"] a:last-of-type', {timeout: 5000})).click();

    await page.waitForTimeout(1000);

    await (await page.waitForSelector('[data-testid="workouts-pagination"] a:first-of-type', {timeout: 5000})).click();

    await page.waitForTimeout(1000);

    const categorySelect = await page.waitForSelector('[data-testid="category-select"]', {timeout: 5000});

    await categorySelect.click();

    await (await categorySelect.waitForSelector('text="c1"', {timeout: 5000})).click();

    await page.waitForTimeout(5000);
});
