import {BetterSqlite3ConnectionOptions} from 'typeorm/driver/better-sqlite3/BetterSqlite3ConnectionOptions';

const config: BetterSqlite3ConnectionOptions = {
    type: "better-sqlite3",
    database: "database/sqlite3.db",
    migrations: ["database/migrations/*.ts"],
    entities: ["src/workouts/entities/*.ts"],
    cli: {
        migrationsDir: "database/migrations",
        entitiesDir: "src/workouts/entities"
    }
};

export = config;
