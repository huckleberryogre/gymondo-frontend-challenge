import {MigrationInterface, QueryRunner} from "typeorm";
import {Workout} from "../../src/workouts/entities/workout.entity";
import {helpers, lorem, date} from "faker";

const workoutTypes = ['Active Recovery', 'Aerobic Exercise', 'Anaerobic Exercise', 'Boot Camp', 'Circuit', 'Compound Exercises', 'Cool-Down', 'Cross-Training', 'DOMS', 'Dynamic Warm-Up', 'Foam Rolling', 'Functional Moves', 'Heart Rate Zones', 'HIIT', 'Interval Training', 'Isometrics', 'Plyometrics', 'Reps', 'Resistance', 'RPE', 'Sets', 'Steady-State Cardio', 'Strength Training', 'Super Set'];
const categories = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7'];

export class FillWithMockData1629300943182 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const workoutRepo = queryRunner.connection.getRepository(Workout);

        for (let i = 0; i < 1000; i++) {
            const startDate = +date.between('2021-09-01', '2022-01-01');

            await workoutRepo.insert([{
                name: helpers.randomize(workoutTypes),
                description: lorem.sentence(),
                startDate,
                category: helpers.randomize(categories)
            }]);
        }


    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
























